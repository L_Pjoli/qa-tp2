package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer{
    Element[] buffer;
    int pointer = 0;
	
    public Buffer(int capacity){
    	this.buffer = new Element[capacity];
    }

    // returns the content of the buffer in form of a string
    public String toString(){
        String string = "Buffer : [";
        for(int i = 0; i < getCurrentLoad(); i++) {
        	string += buffer[i].getData();
        	if(i != getCurrentLoad() - 1) {
        		string += ", ";
        	}else {
        		string += "]";
        	}
        }
        return string;
    }

    // returns the capacity of the buffer
    public int capacity(){
        return buffer.length;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad(){
    	if(isFull()) {return capacity();}
    	if(isEmpty()) {return 0;}
    	
        for(int i = 0; i < capacity(); i++) {
        	if(buffer[i] == null) {
        		return i;
        	}
        }
        return 0;
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty(){
        if(buffer[0] == null) {
        	return true;
        }
        return false;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull(){
        for(int i = 0; i < capacity(); i++) {
        	if(buffer[i] != null && i + 1 == capacity()) {
        		return true;
        	}
        }
        return false;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException {
        if(isFull()) {
        	throw new BufferFullException();
        }
    	buffer[pointer] = element;
        pointer++;
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException {
        if(isEmpty()) {
        	throw new BufferEmptyException();
        }
        Element elementReturn = buffer[getCurrentLoad() - 1];
    	buffer[getCurrentLoad() - 1] = null;
    	pointer--;
    	return elementReturn;
    	
    }
}
