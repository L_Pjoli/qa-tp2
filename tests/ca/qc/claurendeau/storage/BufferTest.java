package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;


public class BufferTest {

	Buffer buffer, buffer2;
	Element element1, element2, element3;
	
    @Rule
    public final ExpectedException exception = ExpectedException.none();
    
	@Before
	public void init() throws BufferFullException{
		buffer = new Buffer(5);
		buffer2 = new Buffer(5);
		element1 = new Element(1);
		element2 = new Element(4);
		element3 = new Element(10);
    	buffer.addElement(element1);
    	buffer.addElement(element2);
    	buffer.addElement(element3);
	}
	
    @Test
    public void testBuildSuccess() {
        assertTrue(true);
    }
    
    @Test
    public void testCreateBuffer() {
    	assertNotNull(buffer.buffer);
    }
    
    @Test
    public void testCapacity() {
    	assertTrue(buffer.capacity() == 5);
    }
    
    @Test
    public void testAddElement(){
    	assertTrue(buffer.buffer[1].getData() == 4);
    }
    
    @Test
    public void testIsEmpty() {
    	assertTrue(buffer2.isEmpty());
    }
    
    @Test
    public void testIsFull() throws BufferFullException{
    	buffer.addElement(element1);
    	buffer.addElement(element1);
    	assertTrue(buffer.isFull());
    }
    
    @Test
    public void testGetCurrentLoad() throws BufferFullException{
    	buffer.addElement(element1);
    	assertTrue(buffer.getCurrentLoad() == 4);
    }
    
    @Test
    public void testToString() {
    	assertEquals("Buffer : [1, 4, 10]", buffer.toString());
    }
    
    @Test
    public void testRemoveElement() throws BufferEmptyException{
    	assertEquals(element3, buffer.removeElement());
    	assertEquals("Buffer : [1, 4]", buffer.toString());
    }
    
    @Test
    public void testBufferFullException() throws BufferFullException{
    	exception.expect(BufferFullException.class);
    	buffer.addElement(element1);
    	buffer.addElement(element2);
    	buffer.addElement(element3);
    }
    
    @Test
    public void testBufferEmptyException() throws BufferEmptyException{
    	exception.expect(BufferEmptyException.class);
    	buffer2.removeElement();
    }

    @Test
    public void testInitElement() {
    	Element test = new Element();
    	assertNotNull(test);
    }
    
	@Test
	public void testSetData() {
		element1.setData(2);
		assertTrue(element1.getData() == 2);
	}
	
	@Test
	public void testGetData() {
		assertTrue(element1.getData() == 1);
	}
	
	@Test
	public void testGetCurrentLoadFull() throws BufferFullException {
    	buffer.addElement(element1);
    	buffer.addElement(element2);
    	assertTrue(buffer.getCurrentLoad() == 5);
	}
	
	@Test
	public void testGetCurrentLoadEmpty() {
		assertTrue(buffer2.getCurrentLoad() == 0);
	}

}


